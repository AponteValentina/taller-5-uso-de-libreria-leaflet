class Mapa{

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligonos=[];
    rectangulos=[];


    constructor(){

        this.posicionInicial=[4.60619817,-74.11160231];
        this.escalaInicial=14;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor={
            maxZoom:20
        };


        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial,this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL,this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }


    colocarMarcador(posicion){

        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor);
    }

    colocarCirculo(posicion, configuracion){

        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor);

    }

    colocarPoligono(vertice1,vertice2,vertice3){

        this.poligonos.push(L.polygon(vertice1,vertice2,vertice3));
        this.poligonos[this.poligonos.length-1].addTo(this.miVisor);
    }
    
    colocarCirculo(posicion, configuracion){

        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor);

    }

    colocarRectangulo(vertices){
        // create an orange rectangle
        this.rectangulos.push(L.rectangle(vertices, {color: "#ff7800", weight: 1}))
        this.rectangulos[this.rectangulos.length-1].addTo(this.miVisor);
    }


}

let miMapa=new Mapa();
//Sede la 40
miMapa.colocarMarcador([4.62811556,-74.06552464]);
miMapa.colocarCirculo([4.62811556,-74.06552464], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 10
});
miMapa.colocarCirculo([4.62801397,-74.06581432], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 15
});

//Sede la 34
miMapa.colocarMarcador([4.62202538,-74.06843752]);
miMapa.colocarCirculo([4.62202538,-74.06843752], {
    color: 'blue',
    fillColor: '#03f',
    fillOpacity: 0.5,
    radius: 10
});

//Sede Macarena A
miMapa.colocarMarcador([4.6138712,-74.06341642]);
miMapa.colocarRectangulo([[4.6138712,-74.06426936], [4.61276436,-74.06304091]]);
miMapa.colocarRectangulo([[4.61454493,-74.06353444], [4.61380704,-74.06304091]]);

//Sede Macarena B
miMapa.colocarMarcador([4.61340066,-74.06500429]);
miMapa.colocarRectangulo([[4.61393002,-74.06569093], [4.61306915,-74.06495601]]);


//Sede vivero
miMapa.colocarMarcador([4.59722569,-74.06445444]);
miMapa.colocarRectangulo([[4.59763742,-74.06463414], [4.59670167,-74.06380802]]);

//Sede Aduanilla de Paiba
miMapa.colocarMarcador([4.61477485,-74.09340084]);
miMapa.colocarCirculo([4.61477485,-74.09340084], {
    color: 'green',
    fillColor: '#3f0',
    fillOpacity: 0.5,
    radius: 50
});

//Sede Bosa Porvenir
miMapa.colocarMarcador([4.63693259,-74.18624818]);
miMapa.colocarRectangulo([[4.63760629,-74.18680876], [4.63628027,-74.18558031]]);

//Sede Facultad Tecnológica 
miMapa.colocarMarcador([4.57903442,-74.15754855]);
miMapa.colocarPoligono([[4.57994346,-74.15920079],[4.57842483,-74.15819228],[4.57900234,-74.15691555]]);
